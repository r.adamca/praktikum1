<?php 
$test=false;
$c=array('','','',4,5,6,7,8,9,4,5,6,7,8,9,1,2,3,7,8,9,1,2,3,4,5,6,2,3,4,5,6,7,8,9,1,5,6,7,8,9,1,2,3,4,8,9,1,2,3,4,5,6,7,3,4,5,6,7,8,9,1,2,6,7,8,9,1,2,3,4,5,9,1,2,3,4,5,6,7,8);


function transform_to_1D($a){
  $x=array();
  foreach($a as $v)
    foreach($v as $w)
      $x[]=$w;
  return $x;
  
  }
  
  function transform_to_2D($a){
  $x=array();
  for($i=0; $i<=8; $i++)
    for($j=0;$j<=8; $j++)
      $x[$i][$j]=$a[$i*9+$j];
  return $x;
}



function tester_1D($c){
  $test=true;
  for($i=0; $i<=8; $i++){
    $sum_zeile=$sum_spalte=0;
    for($j=0;$j<=8; $j++){
      if(!($c[$i*9+$j]>0 && $c[$i*9+$j]<=9))
	$test=false;
      $sum_zeile  += $c[$i*9+$j];
      $sum_spalte += $c[$j*9+$i];
    }
    
    if($sum_zeile != 45 || $sum_spalte != 45)
      $test=false;
  }
  return $test;
}

function print_arr($a){
 foreach ($a as $v) echo "$v,";
 echo "\n";
 }

function fill_arr($c){
 foreach ($c as $key=>$value)
  if($value=='')
   $c[$key]=1;
  return $c; 
}

function solve($a,$c, $i){
  if($i<0)
    return $c;  
  if($a[$i]>0){
     $c=solve($a,$c, $i-1);
    }
  else
    $c[$i]++;
  
  if($c[$i]==10){
    $c[$i]=1;
    $i--;
    $c=solve($a,$c, $i);
 }
  return $c;
}

function solver($a, $c){
  $test=false;
  while(!$test){
  $test=tester_1D($c);
  if($test)
   return $c;
  $c=solve($a,$c,count($c)-1);
  }
  return $c;
  }
  
 
$c=transform_to_2D($c);

if(isset($_POST['submit'])){
  $c=transform_to_1D($_POST['ar']);
  foreach($c as $v)
    echo "$v,";
  $test=tester_1D($c);
  $c=transform_to_2D($c);
}

if(isset($_POST['solver'])){
  $a=transform_to_1D($_POST['ar']);
  $c=fill_arr($a);
  
  $time_start = microtime(true);
  $result=solver($a, $c);
  $time_end = microtime(true);

  $time = $time_end - $time_start;
  $c=transform_to_2D($result);
}
?>
<!DOCTYPE html>
<html>
  <head>
   <title>Spieleseite:Sudoku</title>
 <!-- Latest compiled and minified CSS -->
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">

<!-- Optional theme -->
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css" integrity="sha384-rHyoN1iRsVXV4nD0JutlnGaslCJuC7uwjduW9SVrLvRYooPp2bWYgmgJQIXwl/Sp" crossorigin="anonymous">

<!-- Latest compiled and minified JavaScript -->
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script> 
  </head>
    <body>
 <!-- Fixed navbar -->
    <nav class="navbar navbar-inverse navbar-fixed-top">
      <div class="container">
        <div class="navbar-header">
          <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
          <a class="navbar-brand" href="#">Bootstrap theme</a>
        </div>
        <div id="navbar" class="navbar-collapse collapse">
          <ul class="nav navbar-nav">
            <li class="active"><a href="#">Home</a></li>
            <li><a href="#about">About</a></li>
            <li><a href="#contact">Contact</a></li>
            <li class="dropdown">
              <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Dropdown <span class="caret"></span></a>
              <ul class="dropdown-menu">
                <li><a href="#">Action</a></li>
                <li><a href="#">Another action</a></li>
                <li><a href="#">Something else here</a></li>
                <li role="separator" class="divider"></li>
                <li class="dropdown-header">Nav header</li>
                <li><a href="#">Separated link</a></li>
                <li><a href="#">One more separated link</a></li>
              </ul>
            </li>
          </ul>
        </div><!--/.nav-collapse -->
      </div>
    </nav>

    <div class="container theme-showcase" role="main">

      <!-- Main jumbotron for a primary marketing message or call to action -->

    <div class="page-header">
      
       <h1 style="font-family:'Baloo Bhaina',cursive; color:#FF00FF" >Sudoku</h1>
      <?php
      if($test) 
	echo "Alles Richtig";
      else 
	echo "Fehler"; 
      ?>
      
        <h3><?php if(isset($time)) echo "Execution Time: ".(number_format($time,2,',','.'))." sec"; ?></h3>
      
      
    </div>

  <div class="row">

    <form action="" method="POST">
    <?php for($i=0;$i<9;$i++){ ?>
      <div class="col-xs-2"></div>
      <?php for($j=0; $j<9; $j++) {
	  echo'<div class="col-xs-1" style="'.(($j==2 || $j==5) ? 'border-right: 1px solid grey;' : '').(($i==2 || $i==5) ? 'border-bottom: 1px solid grey;' : '').'">
		<div class="form-group">
		  <input type="text" class="form-control" name="ar['.$i.'][]" value="'.(isset($c[$i][$j]) ?$c[$i][$j] : '').'" />
		</div>
	      </div>';
	   }
	?>
	<div class="col-xs-1"></div>
	<?php } ?>
    <button type="submit" class="btn btn-success" name="submit" >Submit</button>
    <button type="submit" class="btn btn-danger" name="solver" >Solver</button>
 
 </form>
      
    </div>
  </div>

    <!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
    <script>window.jQuery || document.write('<script src="../../assets/js/vendor/jquery.min.js"><\/script>')</script>
    <script src="../../dist/js/bootstrap.min.js"></script>
    <script src="../../assets/js/docs.min.js"></script>
    <!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
    <script src="../../assets/js/ie10-viewport-bug-workaround.js"></script>
  </body>
</html>