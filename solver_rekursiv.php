<?php
$a=array('','','',4,5,6,7,8,9,4,5,6,7,8,9,1,2,3,7,8,9,1,2,3,4,5,6,2,3,4,5,6,7,8,9,1,5,6,7,8,9,1,2,3,4,8,9,1,2,3,4,5,6,7,3,4,5,6,7,8,9,1,2,6,7,8,9,1,2,3,4,5,9,1,2,3,4,5,6,7,8);

function tester_1D($c){
  $test=true;
  for($i=0; $i<=8; $i++){
    $sum_zeile=$sum_spalte=0;
    for($j=0;$j<=8; $j++){
      if(!($c[$i*9+$j]>0 && $c[$i*9+$j]<=9))
	$test=false;
      $sum_zeile  += $c[$i*9+$j];
      $sum_spalte += $c[$j*9+$i];
    }
    if($sum_zeile != 45 || $sum_spalte != 45)
      $test=false;
  }
  return $test;
}

function print_arr($a){
 foreach($a as $v) echo "$v,";
 echo "\n";
}
function fill_arr($c){
  foreach($c as $key=>$value)
    if($value=='')
      $c[$key]=1;
  return $c;
}
function solve($a, $c, $i){
  if($i<0)
    return $c;
  if($a[$i]>0){
    $c=solve($a, $c, $i-1);
  }
  else
    $c[$i]++;
  if($c[$i]==10){
    $c[$i]=1;
    $i--;
    $c=solve($a, $c, $i);
  }
  return $c;
}

function solver($a, $c){
  $test=false;
  while(!$test){
    $test=tester_1D($c);
    if($test)
      return $c;
    $c=solve($a,$c,count($c)-1);
    print_arr($c);
  }
  return $c;
}
$c=fill_arr($a);
print_arr($c);

$result=solver($a, $c);